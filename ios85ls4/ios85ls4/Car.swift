//
//  Car.swift
//  ios85ls4
//
//  Created by WA on 13.04.2021.
//

import UIKit

enum BodyType: String {
    case sedan = "Mega-Sedan"
    case coope, universal
}

class Car {
    let make: String
    let bodyType: BodyType
    var color: UIColor = .black

    init(make: String = "Toyota", bodyType: BodyType) {
        self.make = make
        self.bodyType = bodyType
    }

    init() {
        make = "Toyota"
        bodyType = .sedan
        color = .green
    }

    init(make: String) {
        self.make = make
        bodyType = .sedan
        color = .green
    }

    deinit {
        print(make, "is destroyed")
    }

    func startEngine() {
        print("RR---rRRrrrr")
    }
}
