import UIKit

class ViewController: UIViewController {
    
    var someCar: Car? = Car(make: "Mazda")

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        target(count: 6)
//        piramid(num: 5)
//        centeredView(counter: 5)
//        let toyotaCar = Car()
//
//        print(someCar?.make)
//        print(toyotaCar.bodyType.rawValue)
//
//        let astraCar = Car(make: "Opel", bodyType: .coope)
//        print(astraCar.make, astraCar.bodyType)
//        astraCar.startEngine()
//
//        astraCar.color = .red
//
//        let battleCar = BattleCar(gunType: .minigun)
//        print(battleCar.make, battleCar.bodyType, battleCar.gunType)
//        battleCar.startEngine()
//        battleCar.fire()
//        print(battleCar.gunType, battleCar.gunType.damage, GunType.rocketLauncher.damage)
//
//        let battleCarTwo = BattleCar(gunType: .rocketLauncher)
//        battleCarTwo.fire()
//        someCar = nil
        passwordChecker(password: "123qwE#")
        passwordChecker(password: "123")
        passwordChecker(password: "1233sW,")
    }
    
//    Задача 6. Проверить пароль на надежность от 1 до 5
//
//    a) если пароль содержит числа +1
//
//    b) символы верхнего регистра +1
//
//    c) символы нижнего регистра +1
//
//    d) спец символы +1
//
//    e) если содержит все вышесказанное
//
//    Пример:
//
//    123456 – 1 a)
//
//    qwertyui – 1 c)
//
//    12345qwerty – 2 a) c)
//
//    32556reWDr – 3 a) b) c)
    func passwordChecker(password: String) -> Bool {
        if password.isEmpty {
            return false
        }
        let allowedSymbols: [String.Element] = [",", "+", "-"]
        var counter = 0
        let char = password.first { char -> Bool in
            return char.isNumber
        }
        if char != nil {
            counter += 1
        }
        let uppercased = password.first { char -> Bool in
            return char.isUppercase
        }
        if uppercased != nil {
            counter += 1
        }
        let lowercased = password.first { char -> Bool in
            return char.isLowercase
        }
        if lowercased != nil {
            counter += 1
        }
        let symbol = password.first { char -> Bool in
            return allowedSymbols.contains(char)
//            return char.isSymbol || char.isMathSymbol || char.isPunctuation
        }
        if symbol != nil {
            counter += 1
        }
//        for char in password {
//            if char.isNumber {
//                counter += 1
//            }
//        }
        print("password: \(password) is \(counter == 4 ? "Valid" : "Invalid")")
        return counter == 4
    }
    
    func target(count: Int){
        let padding: CGFloat = 10
        let size: CGFloat = 100
        for i in 0..<count {
            addBox(x: padding + (padding + size) * CGFloat(i), y: 20, size: size)
        }
    }

    func centeredView(counter: Int) {
        let padding: CGFloat = 20
        let size: CGFloat = 200
//        for i in 0..<counter {
//            let floatI = CGFloat(i)
//            let boxView = addBox(x: padding * floatI, y: padding * floatI, size: size - floatI * padding * 2)
//            boxView.backgroundColor = i % 2 == 0 ? .red : .blue
//        }
        for i in 0..<counter {
            let center = view.center
            let newSize = size - padding * CGFloat(i)
            addCenteredView(center: center, size: newSize, color: i % 2 == 0 ? .red : .blue)
        }
    }

    func addCenteredView(center: CGPoint, size: CGFloat, color: UIColor) {
        let box = UIView(frame: CGRect(x: 0, y: 0, width: size, height: size))
        box.center = center
        box.backgroundColor = color
        view.addSubview(box)
    }

    func addBox(x: CGFloat, y: CGFloat, size: CGFloat) -> UIView {
        let box = UIView(frame: CGRect(x: x, y: y, width: size, height: size))
        box.backgroundColor = #colorLiteral(red: 0.09724163264, green: 0.09505683929, blue: 1, alpha: 1)
        view.addSubview(box)
        return box
    }
}

